
Pod::Spec.new do |s|

  s.name         = "HATest"
  s.version      = "0.0.1"
  s.summary      = "我只是测试而已summary"

  s.description  = <<-DESC
                    我只是测试而已summary。。。
                   DESC

  s.homepage     = "http://www.jianshu.com/p/d58a3ac8ef4e"
  

  s.license      = "Copyright (c) 2016年 Lisa. All rights reserved."
  
  s.author       = { "黄炎桂" => "huangyg@minstone.cn" }
  
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitlab.com/huangyg/HANative.git", :tag => "#{s.version}" }

  s.source_files  = "HALog", "*.{h,m}"

  # s.source_files  = "Classes", "Classes/**/*.{h,m}"
  # s.exclude_files = "Classes/Exclude"

  # s.public_header_files = "Classes/**/*.h"

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
