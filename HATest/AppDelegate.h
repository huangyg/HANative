//
//  AppDelegate.h
//  HATest
//
//  Created by yans67 on 16/10/28.
//  Copyright © 2016年 Minstone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

